/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seguntos;

/**
 *
 * @author davidpm
 */
public class Tiempo {
    private int totalSegundos;
    private int segundos;
    private int minutos;
    private int horas;
    private int dias;
    @Override
    public String toString() {
        return "Tiempo{" + "totalSegundos=" + 
        totalSegundos + ", segundos=" + segundos +
        ", minutos=" + minutos + ", horas=" +
                horas + ", dias=" + dias + '}';
    }

        public Tiempo(int Segundos) {
        this.totalSegundos = Segundos;
        repartoSegundos();
    }
    private void repartoSegundos(){
    this.segundos = this.totalSegundos;
    this.dias = this.segundos / 86400;
    this.segundos = this.segundos % 86400;
    this.horas = this.segundos / 3600;
    this.segundos = this.segundos % 3600;
    this.minutos = this.segundos / 60;
    this.segundos = this.segundos % 60;
    }
    public int getTotalSegundos() {
        return totalSegundos;
    }

    public void setTotalSegundos(int totalSegundos) {
        this.totalSegundos = totalSegundos;
    }

    public int getSegundos() {
        return segundos;
    }

    public void setSegundos(int segundos) {
        this.segundos = segundos;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }
  
}
