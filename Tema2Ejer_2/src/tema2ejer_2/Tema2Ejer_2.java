package tema2ejer_2;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
public class Tema2Ejer_2 {
    public static void main(String[] args) {
        Scanner t = new Scanner(System.in);
        System.out.println("Introduzca rango para las matrices cuadradas.");
        int rango = t.nextInt();
        try {
            Matriz matriz0 = new Matriz(rango);
            Matriz matriz1 = new Matriz(rango);
            int hasta = matriz0.getRango();
            System.out.println("Valores para la primera matriz.");
            for (int i = 0; i < hasta; i++) {
                for (int j = 0; j < hasta; j++) {
                    System.out.println("Introduce un número real.");
                    double valor = t.nextDouble();
                    matriz0.setValor(valor, i, j);
                }
            }
            System.out.println("Valores para la segunda matriz.");
                for (int i = 0; i < hasta; i++) {
                for (int j = 0; j < hasta; j++) {
                    System.out.println("Introduce un número real.");
                    double valor = t.nextDouble();
                    matriz1.setValor(valor, i, j);
                }
            }
            System.out.println("Comprovación si alguna de estas matrices o ambas es simetrica:.");
            if(matriz0.esSimetrica()){
                System.out.println("La primera matriz es simetrica.");
            }else{
                System.out.println("La primera matriz NO es simetrica.");
            }
            if(matriz1.esSimetrica()){
                System.out.println("La segunda matriz es simetrica.");
            }else{
                System.out.println("La segunda matriz NO es simetrica.");
            }
        //    System.out.println("Suma de las dos matrices: ");
          //  matriz0.suma(matriz1);   
        } catch (Matriz.IlegalArgumeteException ex) {
            Logger.getLogger(Tema2Ejer_2.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }
}
