package tema2ejer_2;
import java.util.logging.Level;
import java.util.logging.Logger;
public class Matriz {
    private double[][] matriz;
    String msj;
    public Matriz(int rango) throws IlegalArgumeteException{
        if(rango<= 0){
        msj = "La matriz no puede tener 0 elemtos ni elemtos negativos.";
        throw new IlegalArgumeteException(" No se pudo iniciar clase  matriz.\n"+msj);
        }
        this.matriz= new double[rango][rango];
    }
    public Matriz(double[][] matriz) throws IlegalArgumeteException{
        
        if(!(matriz.length == matriz[0].length)){
            throw new IlegalArgumeteException("La matriz introducida no es cuadrada.");
        }else{
        this.matriz = matriz;
        }
    } 
    public Matriz(Matriz matriz) {
        this.matriz = matriz.getMatriz();
    }
    public int getRango(){
        return this.matriz.length;
    }
    public double getValor(int fila, int columna)throws dexOutOfBoundsExeception {
        if(this.matriz.length <= fila && this.matriz[0].length <= columna){
            throw new dexOutOfBoundsExeception("Los parametros no son correctos.");
        }else{
        return this.matriz[fila][columna];
        }
    }
    public void setValor(double valor, int fila, int columna)throws indexOutOfBoundsExeception{
        if(this.matriz.length <= fila && this.matriz[0].length <= columna){
            throw new indexOutOfBoundsExeception("indexOutOfBoundsExeception");
        }else{
        this.matriz[fila][columna] = valor;
        }
    }
    public void suma(Matriz sumar) throws IlegalArgumeteException{
                for(int i = 0;i < this.matriz.length; i++){
                    System.out.println("Fila "+i +":");
            for(int j = 0; j < this.matriz[0].length; j++){
        double resultado = sumar.getValor(i, j) + this.getValor(i, j);
                System.out.print(" " + resultado);
            }
        }
    }
    public double[][] getMatriz() {
        return matriz;   
    }
    public Matriz getTraspuesta(){
     Matriz matrizTraspuesta = null;
        int iTraspuesta = -1;
        int jTraspuesta = -1;
        double nuevoValor = 0.0;
        for(int i = this.matriz.length; i> 0; i-- ){ iTraspuesta++;
            for(int j = this.matriz.length; j> 0; j--){jTraspuesta++;
            nuevoValor = this.matriz[i][j];
            try {
                matrizTraspuesta.setValor(nuevoValor, iTraspuesta, jTraspuesta);
            } catch (indexOutOfBoundsExeception ex) {
                Logger.getLogger(Matriz.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        }
        return matrizTraspuesta;
    }
    public boolean esSimetrica(){
        boolean simetrica = true;
        //Matices impares no pueden ser simetricas
        if(this.getRango()%2==0){
        simetrica = this.continuaSimetria();
        }else{
        simetrica = false;    
        }
        return simetrica;
    }
    private boolean continuaSimetria(){
        boolean simetrica = true;
        int jDerecha = ((this.getRango()/2)-1);
        
        for(int i = 0; i >= this.getRango() ; i++){
            for(int jIzquierda = ((this.getRango()/2)-1); jIzquierda >= 0; jIzquierda--){
                
                if((this.matriz[i][jIzquierda]==this.matriz[i][jDerecha])){
                    simetrica = true;
                    System.out.println("if:");
                    System.out.println("Fila I("+ i +"),columna J("+ jIzquierda +") => "+this.matriz[i][jIzquierda]+".");
                    System.out.println("Fila I("+ i +"),columna J("+ jDerecha +") => "+this.matriz[i][jDerecha]+".\n");
                }else{
                    System.out.println("else:");
                    System.out.println("Fila I("+ i +"),columna J("+ jIzquierda +") => "+this.matriz[i][jIzquierda]+".");
                    System.out.println("Fila I("+ i +"),columna J("+ jDerecha +") => "+this.matriz[i][jDerecha]+".\n");
                    simetrica = false;
                    break;
                }
                jDerecha++;
            }
        }
        return simetrica;
    }
    public String toString() {
        String frase = "Números correspondientes a la matriz:\n ";
        int hasta = this.matriz.length;
        for(int i = 0; i < hasta; i++){
            for(int j = 0; j < hasta; j++){
                frase += "Fila " + i +", columna "+ j +" -> " + this.matriz[i][j] +".\n";
            }
        }
        return frase;
    }
    public void setMatriz(double[][] matriz) {
        this.matriz = matriz;
    }
        // Clase anidada, para emplear menos capturas.
    public class IlegalArgumeteException extends Exception{
    public IlegalArgumeteException(String mensaje){
        super(mensaje);
    }
}
    public class dexOutOfBoundsExeception extends IlegalArgumeteException{
        public dexOutOfBoundsExeception(String mensaje){ 
            super(mensaje);
        }
}
     public class indexOutOfBoundsExeception extends dexOutOfBoundsExeception{
        public indexOutOfBoundsExeception(String mensaje){ 
            super(mensaje);
        }
}   
}