/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impares;

/**
 *
 * @author davidpm
 */
public class Contar {
    private int inicio;
    private int paso;
    private int parar;
    public Contar(int inicio, int paso, int parar) {
        this.inicio = inicio;
        this.paso = paso;
        this.parar = parar;
    }
       public void contador(){
        for(int i = inicio; i < parar; i+=paso){
            System.out.println(i);
        }
    }
    public int getInicio() {
        return inicio;
    }

    public void setInicio(int inicio) {
        this.inicio = inicio;
    }

    public int getPaso() {
        return paso;
    }

    public void setPaso(int paso) {
        this.paso = paso;
    }

    public int getParar() {
        return parar;
    }

    public void setParar(int parar) {
        this.parar = parar;
    }


    
    
}
