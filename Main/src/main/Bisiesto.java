package main;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author davidpm
 */
public class Bisiesto {
    private int year;

    public Bisiesto(){
        this.year = 2000;
    }
    
    public Bisiesto(int year) {
        this.year = year;
    }
    public void dimeSiEsBisiesto(){
        if((this.year % 4 == 0 && this.year % 100 != 0)
                || (0 == this.year % 400)){
            System.out.println("El año " + year +
                    " es bisiesto.");
        }else{
            System.out.println("El año " + year +
                    " No es bisiesto.");
        }
    }
    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }


    
}
