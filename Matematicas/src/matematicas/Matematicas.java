/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matematicas;
import java.util.Scanner;
public class Matematicas {
    public static void main(String[] args) {
        Scanner t = new Scanner(System.in);
        System.out.println("Introduce un número entero:");
        int num = t.nextInt();
        Primo primo = new Primo(num);
        primo.respuestaPrimo();
    }
}
class Primo{//En el mismo documento main
    private int num;
    public Primo(int num){
        this.num = num;
    }
    public void respuestaPrimo(){
        boolean primo = true;
        for(int i = 2; i < this.num; i++){
            if(num % i == 0){
            primo = false;
            break;
            }
        }
        if(primo){
            System.out.println("El número "+
                    this.num +" es primo.");
        }else{
            System.out.println("El número "+
                    this.num +" No es primo.");
        }
        
    }
    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

   
}