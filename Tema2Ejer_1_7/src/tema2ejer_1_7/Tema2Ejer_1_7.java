/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tema2ejer_1_7;
import java.util.Scanner;
public class Tema2Ejer_1_7 {
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner t = new Scanner(System.in);
        Fibonacci numFibo = new Fibonacci();
        System.out.println("Ejercicio fibonacci, no"
                + " debes introducir número negativos.");
        System.out.println("Introduce un número:");
        int num = t.nextInt();
        if(num<0){
            System.out.println("Pese la advertencia has"
                    + " introducido número negativo.");
        }else{
        numFibo.miFibonacci(num);
        }

    }
    
}
