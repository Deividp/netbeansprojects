package tema2ejer_1_9;
public class MCD {
    private int mayor;
    private int menor;
    public MCD(int num0, int num1){
        if(num0>num1){
        this.mayor = num0;
        this.menor = num1;
    }else{
        this.mayor = num1;
        this.menor = num0;   
        }
        }
        public String getMCD(){
        int dividendo = this.mayor;
        int divisor = this.menor;
        int resto;
            do {                
                resto = dividendo % divisor;
                if(resto!=0){
                dividendo = divisor;
                divisor = resto;
            }
            } while(resto!=0);
        return "El MCM de "+ this.mayor +" y "+
              this.menor +" es: "+ divisor +".";
    }
}
